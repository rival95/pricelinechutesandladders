package priceline.chutesandladders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ChutesAndLadders {
	private static final int	MIN_SPIN	= 1;
	private static final int	MAX_SPIN	= 6;
	private static final int	END_SQUARE	= 100;
	private static final String	LADDER		= "LADDER";
	private static final String	CHUTE		= "CHUTE";

	private static Map<Integer, Integer> mGameboard;

	private List<Player>	mPlayers;
	private Random			mRandom	= new Random();

	public ChutesAndLadders() {
		mGameboard = initializeGameboard();
		mPlayers = createPlayers();
	}

	private Map<Integer, Integer> initializeGameboard() {
		Map<Integer, Integer> board = new HashMap<Integer, Integer>();
		// LADDERS
		board.put(1, 38);
		board.put(4, 14);
		board.put(9, 31);
		board.put(21, 42);
		board.put(28, 84);
		board.put(36, 44);
		board.put(51, 67);
		board.put(71, 91);
		board.put(80, 100);

		// CHUTES
		board.put(16, 6);
		board.put(47, 26);
		board.put(49, 11);
		board.put(56, 53);
		board.put(62, 19);
		board.put(64, 60);
		board.put(87, 24);
		board.put(93, 73);
		board.put(95, 75);
		board.put(98, 78);

		return board;
	}

	/**
	 * Creates the players and determines the order the players will play the game.
	 * 
	 * @return
	 */
	private List<Player> createPlayers() {
		List<Player> players = new ArrayList<Player>();
		// TODO allow users to create their own players
		players.add(new Player("Eric"));
		players.add(new Player("Paul"));

		players = determineOrderOfPlayer(players);
		return players;
	}

	/**
	 * Everyone spins the spinner. The player with the highest number goes first.
	 * 
	 * @param players
	 *            Players to play the game.
	 * @return the order the Players will play the game.
	 */
	private List<Player> determineOrderOfPlayer(List<Player> players) {
		Map<Integer, List<Player>> order = new HashMap<Integer, List<Player>>();

		// have each player spin the spinner and record their spin
		for (Player player : players) {
			int spin = spinSpinner();

			if (!order.containsKey(spin)) {
				order.put(spin, new ArrayList<Player>());
			}

			order.get(spin).add(player);
		}

		List<Player> playOrder = new ArrayList<>(players.size());

		// re-arrange players in order of the highest number spun
		for (int i = MAX_SPIN; i >= MIN_SPIN; i--) {
			List<Player> pl = order.get(i);
			if (pl == null) continue;

			for (Player p : pl) {
				playOrder.add(p);
			}
		}

		return playOrder;
	}

	public void beginPlay() {
		Player winner = null;
		int turnCounter = 1;

		while (true) {
			for (int i = 0; i < mPlayers.size(); i++) {
				Player player = mPlayers.get(i);
				int move = spinSpinner();
				movePlayer(player, move, turnCounter++);

				// play continues until a player lands on the END_SQUARE
				if (player.getPosition() == END_SQUARE) {
					winner = player;
					break;
				}
			}
			if (winner != null) break;
		}
		System.out.println("The winner is " + winner.getName() + "!");
	}

	/**
	 * Moves the specified player the number of squares depicted by the <i>move</i> variable, if possible. Then slides the player up a ladder or down
	 * a chute, if needed.
	 * 
	 * @param player
	 *            The Player to be moved.
	 * @param move
	 *            The number of squares the player should be moved.
	 * @param turnCounter
	 *            The total number of turns taken in the game so far.
	 */
	private void movePlayer(Player player, int move, int turnCounter) {
		int tempPos = player.getPosition() + move;
		// player must land on the END_SQUARE exactly, otherwise don't move
		tempPos = tempPos > END_SQUARE ? player.getPosition() : tempPos;
		// determine if the new square has a ladder or chute
		int slide = mGameboard.containsKey(tempPos) ? mGameboard.get(tempPos) : -1;

		printMove(player, tempPos, slide, turnCounter);
		// slide the player if slide is available, otherwise no slide
		player.setPosition(slide == -1 ? tempPos : slide);
	}

	private void printMove(Player player, int tempPos, int slide, int turnCounter) {
		StringBuilder sb = new StringBuilder();
		sb.append(turnCounter + ": " + player.getName() + ": " + player.getPosition() + " --> " + tempPos);

		if (slide != -1) {
			sb.append(" --");
			sb.append(slide < tempPos ? CHUTE : LADDER);
			sb.append("--> " + slide);
		}
		System.out.println(sb.toString());
	}

	/**
	 * @return The number of squares the Player should move between {@code MIN_SPIN} and {@code MAX_SPIN}.
	 */
	private int spinSpinner() {
		return mRandom.nextInt((MAX_SPIN - MIN_SPIN) + 1) + MIN_SPIN;
	}

}
