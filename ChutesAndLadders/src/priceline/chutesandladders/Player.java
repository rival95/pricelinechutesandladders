package priceline.chutesandladders;

public class Player {
	private String	mName;
	private int		mPosition	= 0;

	public Player(String name) {
		mName = name;
	}

	/**
	 * @return the Player's position
	 */
	public int getPosition() {
		return mPosition;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(int position) {
		mPosition = position;
	}

	/**
	 * @return the Player's name
	 */
	public String getName() {
		return mName;
	}

	@Override
	public String toString() {
		return mName + "--" + mPosition;
	}
}
