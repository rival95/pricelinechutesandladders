# **Chutes and Ladders** #
## Interview Assignment ##

Code a simulation in Java of the classic children’s game Chutes And Ladders (or Snakes and Ladders, as it is known in the U.K.). Use the attached instructions as a business spec.

### [Link to Requirements](https://bitbucket.org/rival95/pricelinechutesandladders/src/master/ChutesAndLadders/priceline_chutesandladders.pdf)